<%-- 
    Document   : index
    Created on : Jan 21, 2020, 2:08:08 PM
    Author     : estefani
--%>
<%@page import="index.convertor"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="index.*" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Ejercio 6. ARN TO ADN </title>
    </head>
    <body>
        <h1>Conversió d'ADN a ARN i viceversa. </h1>
        <form method='post' action="index.jsp">
            Select option:
            <select name="tipe">
                <option value="ADN">ADN</option>
                <option value="ARN">ARN</option>
            </select>
            <br>
            Enter sequence: <input type="text" name="sequence" />
            <br>
            <input type="submit" name="ok" value="Convertir"/>
        </form>

        <%
            if (request.getParameter("ok") != null) {
                String type, cadena;

                type = request.getParameter("tipe");
                cadena = request.getParameter("sequence");
                //out.println(type+cadena);
                //out.println(convertor.isADN(cadena));
                //out.println(type);
                if (convertor.isADN(cadena) || convertor.isARN(cadena)) {
                    switch (type) {
                        case "ADN":
                            out.println(convertor.adnToArn(cadena));
                            break;
                        case "ARN":
                            out.println(convertor.arnToAdn(cadena));
                            break;

                    }
                } else {
                    out.println("Has d'introduir bases nitrogenadas A,G,C,T,U");
                }

            }
        %>

    </body>
</html>
