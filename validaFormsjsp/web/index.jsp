<%-- 
    Document   : index
    Created on : Jan 24, 2020, 11:23:30 AM
    Author     : estefani
--%>
<%@page import="index.Validation"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="index.*" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <form method='post' action="">
            <h2>Escribe un comentario</h2>
            <input type="text" name="nombre" /> Nombre (requerido)
            <br>
            <input type="text" name="email" /> E-mail (no será publicado)(requerido)
            <br>
            <input type="text" name="sitioweb" /> Sitio Web 
            <br>
            <textarea name="comment" rows="10" cols="50" ></textarea>
            <br>
            <input type="submit" name="ok" value="Enviar"/>
            <br>

            <input type="checkbox" name="commentarioscorreo" value="true" > 
            Recibir subsiguientes comentarios por correo
            </input>
        </form>
        <%

            if (request.getParameter("ok") != null) {
                Validation val = new Validation();

                String[] commentarios;
                String nombre, email, web, comment;
                nombre = request.getParameter("nombre");
                email = request.getParameter("email");
                web = request.getParameter("sitioweb");
                comment = request.getParameter("comment");

                if (request.getParameterValues("commentarioscorreo") != null && val.validaNoNull(nombre) && val.validaEmpty(nombre) == false && val.validaCampEmail(email) && val.validaCampDireccioWeb(web) && val.validaNoNull(comment) && val.validaEmpty(comment) == false) {
                    commentarios = request.getParameterValues("commentarioscorreo");
                    request.getRequestDispatcher("validar.jsp").forward(request, response);
                } else {
                    out.println("Hi ha algún camp buit.");
                    out.println("Nos has clicat cap commentarios correo");
                }

            } else {

            }


        %>
    </body>
</html>
