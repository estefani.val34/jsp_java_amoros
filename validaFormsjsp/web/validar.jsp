<%-- 
    Document   : validar
    Created on : Jan 24, 2020, 11:27:09 AM
    Author     : estefani
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>

        <%
            String name = request.getParameter("nombre");
            session.setAttribute("snom", name);
            String email = request.getParameter("email");
            String sitioweb = request.getParameter("sitioweb");
            String comment = request.getParameter("comment");
            String[] commentarioscorreo;
            commentarioscorreo = request.getParameterValues("commentarioscorreo");

            out.println("Name: " + name+"<br/>" +"Email: "+email+"<br/>" +"Sitio web: "+sitioweb+"<br/>" +"Comment: "+comment+"<br/>" );
            out.println("Commentarios correo: "+commentarioscorreo[0]);
            
      %>
    </body>
</html>
