
package index;

/**
 * Aquesta classe calcula l'index de massa corporal d'un individu
 * @author alumne
 * @version 1.0, 2018-11-14
 */
public class CalculArees {
    private double base;
    private double alsada;
    private double area;
    private double perimetre;

    public double getBase() {
        return base;
    }

    public void setBase(double base) {
        this.base = base;
    }

    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public double getPerimetre() {
        return perimetre;
    }

    public void setPerimetre(double perimetre) {
        this.perimetre = perimetre;
    }
    
    public CalculArees(double pes, double alsada) {
        this.base = pes;
        this.alsada = alsada;
    }

    public double getPes() {
        return this.base;
    }

    public double getAlsada() {
        return this.alsada;
    }

    public void setPes(double pes) {
        this.base = pes;
    }

    public void setAlsada(double alsada) {
        this.alsada = alsada;
    }
    
    /**Mètode que fa el càlcul de l'index a partir de l pes i l'alçada
     * 
     * @return index de massa corporal
     */
    public double calculaAreaRectangle(){
        this.area=this.base * this.alsada;
        return this.area;
    }
    
    public double calculaPerimetreRectangle(){
        this.area= 2 * this.base + 2* this.alsada;
        return this.area;
    }
    
    public double calculaAreaTriangle(){
        this.area= (this.base * this.alsada) / 2;
        return this.area;
    }
    
    public double calculaPerimetreTriangle(){
        this.area= 3 * this.base;
        return this.area;
    }
    
    /** Mètode que et diu la base i el perímetre d'un rectangle a partir de les dades introduides.
     * 
     * @return una cadena amb el valor que correspon a la base i el perímetre d'un rectangle.
     */
    public String calculsRectangle(){
       StringBuilder resultat = new StringBuilder();
       resultat.append("La àrea de la teva plantació és ");
       resultat.append(calculaAreaRectangle());
       resultat.append("\n I el perímetre és ");
       resultat.append(calculaPerimetreRectangle());
       return resultat.toString();   
    }
    
}
