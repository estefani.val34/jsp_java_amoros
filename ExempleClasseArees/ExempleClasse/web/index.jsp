<%-- 
    Document   : index
    Created on : Nov 13, 2018, 4:17:45 PM
    Author     : alumne
--%>

<%@page import="index.Validation"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="index.*" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>I.M.C.</title>
    </head>
    <body>
       
        <h2>Anem a calcular l'àrea i perimetre de la teva plantació 
            <strike>de coca</strike> 
            de cultius naturals.</h2>
        <form method="post" action="index.jsp">
            <p>Forma:
            <select name="formaPlantacio">
                <option value="Rectangle">Rectangle</option>
                <option value="Triangle">Triangle Equilàter</option>
                <option value="Cercle">Cercle</option>
            </select>
            </p>
            <p>Base (en metres):<input type="text" name="base" />
            </p>
            <p>Alçada (en metres)<input type="text" name="alsada" />
            </p>
            <input type="submit" name="ok" value="Enviar"/>
        </form>  
        
        <%
           
            //si ha clicat o no al botó del formulari
           if(request.getParameter("ok")!=null) {
              
              double base, alsada;
              String formaPlantacio = request.getParameter("formaPlantacio");
              //debería validar los valores de mis cajas
              //según lo entrado, me debería salir o no un resultado
              //crear una clase de validacion
              out.println("Forma plantació " + formaPlantacio);
              out.println();
              
              base=Validation.validaDouble(request.getParameter("base"));
              alsada=Validation.validaDouble(request.getParameter("alsada"));
             
              if(base <0 || alsada <0){
                  out.println("Has d'introduir números positius");
              }else{
                  
                CalculArees resultatArees = new CalculArees(base,alsada);
                
                switch(formaPlantacio) {
                    case "Rectangle":
                        out.println("Àrea " +resultatArees.calculaAreaRectangle());
                        out.println("Perímetre " + resultatArees.calculaPerimetreRectangle());
                        break;
                    case "Triangle":
                        out.println("Àrea " + resultatArees.calculaAreaTriangle());
                        out.println("Perímetre " + resultatArees.calculaPerimetreTriangle());
                         break;
                    case "Cercle":
                        out.println("No tenim la calculadora disponible");
                         break;
                    default: out.println("Selecciona una forma");
                }
                // out.println("El teu Index de massa corporal és de: <br/>");
                // out.println(String.format("%.2f", indice.ca()));
                // out.println(resultatArees.calculsRectangle());
                
                // Volem que guardi els valors que haviem posat abans.
                
              }
           }
           
        %>
        
    </body>
</html>
