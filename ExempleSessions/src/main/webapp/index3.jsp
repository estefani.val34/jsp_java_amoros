<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Adquireix els nostres cursos - Pas 1.</title>
</head>
<body>
	<h1>Adquireix els nostres cursos - Pas 1.</h1>
	<form action="pas2.jsp" method="post">
		Introdueix...
		Nom: <input type="text" name="nom">
		<br/>
		Edat: <input type="number" name="edat" min="0" max="150">
		<br/>
		<input type="submit" value="Continuar">
	</form>
</body>
</html>