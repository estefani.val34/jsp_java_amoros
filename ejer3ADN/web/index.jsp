<!DOCTYPE html>
<!--
Fes una JSP que validi una seqüència d’ADN passada per un TextArea.
Una seqüència ADN vàlida només pot tenir caràcters A,G,C i T.
-->
<%@page import="index.ValidationDNA"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="index.*" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Ejercici del 3 al 5</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
       
        
        <h2>VALIDATION FORMAT AND</h2>
        <form method="post" action="index.jsp">
            <p>Write ADN here</p>
            <textarea name="adn" rows="10" cols="50" ></textarea>
            <br>
            <input type="submit" name="ok" value="Enviar"/>
        </form>  
        <%

            if (request.getParameter("ok") != null) {
                String adn_field = request.getParameter("adn");

                if (ValidationDNA.validaDNA(adn_field) == 1) {

                    out.println(adn_field);
                    out.println("  Correcto!<br>");
                    out.println("Num. Adeninas =" + ValidationDNA.numAdenines(adn_field) + "<br>");
                    out.println("Num. Guaninas =" + ValidationDNA.numGuanines(adn_field) + "<br>");
                    out.println("Num. Citosinas =" + ValidationDNA.numCitosines(adn_field) + "<br>");
                    out.println("Num. Timinas =" + ValidationDNA.numAdenines(adn_field) + "<br>");
                } else {
                    out.println(" Error!");
                    out.println("Has introducido alguna letra diferente a A,G,C,T:  ");
                    out.println(adn_field);

                }
                
                if (ValidationDNA.validaDNA(adn_field) == 1) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("<dl>");

                    sb.append(" <dt>Num. Adeninas</dt>");
                    sb.append(ValidationDNA.numAdenines(adn_field));
                    sb.append(" <dt>Num. Guaninas</dt>");
                    sb.append(ValidationDNA.numGuanines(adn_field));
                    sb.append(" <dt>Num. Citosinas</dt>");
                    sb.append(ValidationDNA.numCitosines(adn_field));
                    sb.append(" <dt>Num. Timinas</dt>");
                    sb.append(ValidationDNA.numAdenines(adn_field));

                    sb.append("</dl>");
                    out.println(sb);
                } else {
                    out.println(" Error!");
                }

            }

        %>
    </body>
</html>
