/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package index;

/**
 *
 * @author estefani
 */
public class ValidationDNA {

    /**
     * Fa recompte de totes les A's i retorna la quantitat
     *
     * @return Numero de adenines acumulades a tota la cadena
     */
    public static int numAdenines(String ADN) {
        int a = 0;
        //char[] letter = this.adn.toUpperCase().toCharArray();
        for (int i = 0; i < ADN.length(); i++) {
            if (ADN.toUpperCase().charAt(i) == 'A') {
                a++;
            }
        }
        return a;
    }

    /**
     * Fa recompte de totes les G's i retorna la quantitat
     *
     * @return Numero de adenines acumulades a tota la cadena
     */
    public static int numGuanines(String ADN) {
        int g = 0;
        //char[] letter = this.adn.toUpperCase().toCharArray();
        for (int i = 0; i < ADN.length(); i++) {
            if (ADN.toUpperCase().charAt(i) == 'G') {
                g++;
            }
        }
        return g;
    }

    /**
     * Fa recompte de totes les T's i retorna la quantitat
     *
     * @return Numero de adenines acumulades a tota la cadena
     */
    public static int numTimines(String ADN) {
        int t = 0;
        for (int i = 0; i < ADN.length(); i++) {
            if (ADN.toUpperCase().charAt(i) == 'T') {
                t++;
            }
        }
        return t;
    }

    /**
     * Fa recompte de totes les C's i retorna la quantitat
     *
     * @return Numero de adenines acumulades a tota la cadena
     */
    public static int numCitosines(String ADN) {
        int c = 0;
        for (int i = 0; i < ADN.length(); i++) {
            if (ADN.toUpperCase().charAt(i) == 'C') {
                c++;
            }
        }
        return c;
    }

    public static double validaDNA(String adn) {
        double result = 0;
        try {
            //result=Double.parseDouble(adn);
            int sumaAGCT = numCitosines(adn) + numTimines(adn) + numGuanines(adn) + numAdenines(adn);
            int len_adn = adn.length();
            if (sumaAGCT == len_adn) {
                result = 1;
            }
        } catch (NumberFormatException error1) {

        }

        return result;

    }

}
