

<%-- 
    Document   : index
    Created on : Nov 13, 2018, 4:17:45 PM
    Author     : alumne
    A partir de l’exemple de l’IMC, fes un programa anàleg que, a partir de la base i l’alçada d’un rectangle, 
    calculi la seva àrea i el seu perímetre.
--%>

<%@page import="index.Validation"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="index.*" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Ejercico 1. Càlcul Àrea - Perímetre </title>
    </head>
    <body>
       
        <h1>Anem a calcular Àrea - Perímetre d'un rectangle</h1>
        <form method="post" action="index.jsp">
            Base (en cm): <input type="text" name="base" />
            <br/>
            Alçada (en cm): <input type="text" name="alsada" />
            <br/>
            <input type="submit" name="ok" value="Enviar"/>
        </form>  
       
        <%
           
            //si ha clicat o no al botó del formulari
           if(request.getParameter("ok")!=null) {
              
              double base, alsada;
              //debería validar los valores de mis cajas
              //según lo entrado, me debería salir o no un resultado
              //crear una clase de validacion
              
              base=Validation.validaDouble(request.getParameter("base"));
              alsada=Validation.validaDouble(request.getParameter("alsada"));
             
              if(base <0 || alsada <0){
                  out.println("Has d'introduir números positius");
              }else{
                Imc indice=new Imc(base,alsada);

                out.println("La àrea i el perimetre és: <br/>");
                out.println("Area = ");
                out.println(String.format("%.2f", indice.calculaArea()));
                out.println("<br/> Perimetre = ");
                out.println(String.format("%.2f",indice.calculaPerimetre()));
              }
           }
           
        %>
        
    </body>
</html>
