<%-- 
    Document   : optionForma
    Created on : Jan 14, 2020, 1:50:35 PM
    Author     : estefani

http://localhost:8080/area_perimetre/optionForma.jsp
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="index.Validation"%>
<%@page import="index.*" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Ejercio 2. Select Figure</title>
    </head>
    <body>
        <form method='post'>
            Figure:
            <select name="figure">
                <option value="rectangle">Rectangle</option>
                <option value="triangle">Triangle</option>
                <option value="cercle">Cercle</option>
            </select>
            <br>
            Base (en cm): <input type="text" name="base" />
            <br/>
            Alçada (en cm): <input type="text" name="alsada" />
            <br/>
            Radio(cercle)  (en cm): <input type="text" name="radio" />
            <br/>
            <br>
            <br>
            <input type="submit" name="ok" value="Calcule Area ">
        </form>


        <% //esto es jsp
            if (request.getParameter("ok") != null) {
                // I indice=new Imc(base,alsada);
                double base, alsada, radio;

                base = Validation.validaDouble(request.getParameter("base"));
                alsada = Validation.validaDouble(request.getParameter("alsada"));
                radio = Validation.validaDouble(request.getParameter("radio"));

                if (base < 0 || alsada < 0 || radio < 0) {
                    out.println("Has d'introduir números positius");
                } else {
                    String fig = request.getParameter("figure");//Validation.validaString(
                    switch (fig) {
                        case "rectangle":
                            Imc indice = new Imc(base, alsada);

                            out.println("Figure : " + request.getParameter("figure") + "<br>");
                            out.println("Base : " + base + "<br>");
                            out.println("Alçada : " + alsada + "<br>");
                            out.println("Area : " + indice.calculaAreaFigure("rectangle") + "<br>");
                            break;
                        case "triangle":
                            Imc indice2 = new Imc(base, alsada);

                            out.println("Figure : " + request.getParameter("figure") + "<br>");
                            out.println("Base : " + base + "<br>");
                            out.println("Alçada : " + alsada + "<br>");
                            out.println("Area : " + indice2.calculaAreaFigure("triangle") + "<br>");
                            break;
                        case "cercle":
                            Imc indice3 = new Imc(radio);
                            out.println("Figure : " + request.getParameter("figure") + "<br>");
                            out.println("Radio : " + radio + "<br>");
                            out.println("Area : " + indice3.calculaAreaFigure("cercle") + "<br>");
                            break;

                        default:
                            // code block
                            break;
                    }
                }

            }

        %>
    </body>
</html>
