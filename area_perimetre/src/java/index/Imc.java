package index;

/**
 * Aquesta classe calcula l'index de massa corporal d'un individu
 *
 * @author alumne
 * @version 1.0, 2018-11-14
 */
public class Imc {

    private double base;
    private double alsada;
    private double radio;
    private double area;
    private double perimetro;

    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public double getPerimetro() {
        return perimetro;
    }

    public void setPerimetro(double perimetro) {
        this.perimetro = perimetro;
    }

    public Imc(double base, double alsada) {
        this.base = base;
        this.alsada = alsada;
    }

    public Imc(double radio) {
        this.radio = radio;
    }
    
    public double getBase() {
        return this.base;
    }

    public double getAlsada() {
        return this.alsada;
    }

    public void setBase(double base) {
        this.base = base;
    }

    public void setAlsada(double alsada) {
        this.alsada = alsada;
    }

    public double getRadio() {
        return radio;
    }

    public void setRadio(double radio) {
        this.radio = radio;
    }

    /**
     * Mètode que fa el càlcul de l'àrea rectangle
     *
     * @return area rectangle
     */
    public double calculaArea() {
        this.area = this.base * this.alsada;
        return this.area;
    }

    /**
     * Mètode que fa el càlcul de l'àrea segons la figura
     *
     * @return area
     */
    public double calculaAreaFigure(String forma) {

        switch (forma) {
            case "rectangle":
                this.area = this.base * this.alsada;
                break;
            case "triangle":
                this.area = (this.base * this.alsada) / 2;
                break;
            case "cercle":
                this.area = Math.PI * (this.radio) * (this.radio);
                break;
            default:
                break;
        }

        return this.area;
    }

    /**
     * Mètode càlcul perimetre
     *
     * @return perimetre
     */
    public double calculaPerimetre() {
        this.perimetro = 2 * this.base + 2 * this.alsada;
        return this.perimetro;

    }

}
