<%-- 
    Document   : 01
    Created on : Oct 27, 2017, 1:58:21 PM
    Author     : alumne
--%>

<%@page import="java.util.Calendar"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%!
            final float IVA=21;
            
            float percentatge(float preu) {
                return preu*IVA/100;
            }
        %>
        <%
            out.println("<h1>Hello world</h1>");
            
            for(int i=0; i<10; i++) {
                out.println("<p>"+i+"</p>");
                System.out.println(i);
            }
            
            out.println("El preu de l'IVA de 1.000€ és: " + percentatge(1000));
        %>
        
        <p>El preu de l'IVA és: <%= percentatge(2000) %></p>
        
        <%
            boolean existeix = true;
            if(existeix) {
                out.println("Existeix<br>");
            } else {
                out.println("NO Existeix<br>");
            }
            
            out.println(existeix ? "SI" : "NO");
            
            out.println("<hr/>");
            
            Calendar ara = Calendar.getInstance();
            int hora = ara.get(Calendar.HOUR_OF_DAY);
            
            if((hora>20)||(hora<6)) {
                out.println("Bona nit");
            } else if ((hora>=6) || (hora<=12)) {
                out.println("Bon dia");
            } else {
                out.println("Bona tarda");
            }
            
            out.println("<hr/>");
            
            // Switch
            /*
                Comentaris
            */

            
            String nom = "Jacob";
            
            switch(nom) {
                case "Jacob":
                case "Jaime":
                case "Javier": out.println("Nom bíblic");
                               break;
                default: out.println("Nom NO bíblic");
            }
            int kk;
            for(int i=0; i<10; i++)
                out.println(i);
            
            int i=0;
            while(i<10) {
                out.println(i);
                i+=2; // i=i+2
            }
            
            int numSenar=1, suma=0;
            do {
                suma += numSenar;
                numSenar += 2;
                out.println("La suma es: " + suma + "<br>");
            } while(suma<30);
        %>
    </body> 
</html>
