<%-- 
    Document   : 03
    Created on : Oct 30, 2017, 9:29:53 AM
    Author     : alumne
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%
            // $_SESSION
            session.setAttribute("name", "John");
            
            String[] names = {"Anna", "Didac", "Marc"};
            session.setAttribute("names", names);
        %>
        
        El nom és: <%= session.getAttribute("name") %>
        <br>
        Els noms són:
        <%
            String[] tmp = (String[])session.getAttribute("names");
            for(String nom : tmp) {
                out.println(nom+" ");
            }
        %>
                
    </body>
</html>
