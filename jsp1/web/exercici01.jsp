<%-- 
    Document   : exercici01
    Created on : Oct 30, 2017, 10:27:22 AM
    Author     : alumne
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Calculadora IMC</title>
    </head>
    <body>
        <h1>Calculadora IMC</h1>
        
        <%!
            double imc, pes, alsada;
            
            double calcular_imc(double pes, double alsada) {
                imc = pes / Math.pow(alsada, 2);
                return imc;
            }
        %>
        
        <form method="post">
            Introdueix un pes (kg):
            <input type="number" name="pes">
            <br>Introdueix una alçada (m):
            <input type="number" name="alsada">
            <br><input type="submit" name="enviar" value="Enviar">
        </form>
        
        <%
                try {
                    pes = Double.parseDouble(request.getParameter("pes"));
                    alsada = Double.parseDouble(request.getParameter("alsada"));

                    imc = calcular_imc(pes, alsada);

                    String interpretacio = "";

                    if(imc<18) interpretacio="Magror";
                    if(imc>=18 && imc<25) interpretacio="Corpulència normal";
                    if(imc>=25 && imc<30) interpretacio="Sobrepès";
                    if(imc>=30 && imc<40) interpretacio="Obesitat";
                    if(imc>=40) interpretacio="Obesitat mòrbida";

                    out.println("El IMC és: " + String.format("%.2f", imc));
                    out.println("<br>" + interpretacio);
                } catch(NumberFormatException e) {
                    out.println("Introdueix els valors demanats");
                }
        %>
    </body>
</html>
