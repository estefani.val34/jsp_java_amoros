<%-- 
    Document   : 02
    Created on : Oct 27, 2017, 2:42:11 PM
    Author     : alumne

http://localhost:8080/jsp2/02.jsp
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <form method='post'>
            Username: <input type="text" name="username">
            <br>
            Email Address: <input type="email" name="email">
            <br>
            Password: <input type="password" name="password">
            <br>
            Comments: <textarea name="comments"></textarea>
            <br>
            Email format:
            <input type="radio" name="emailformat" value="Plain Text" checked>
            Plain Text
            <input type="radio" name="emailformat" value="HTML">
            HTML
            <br><br>
            Occupation:
            <select name="occupation">
                <option value="webdeveloper">Web Developer</option>
                <option value="dbadministrator">Database Administrator</option>
            </select>
            
            <br>
            Other Languages:
            <input type="checkbox" name="languages" value="php">PHP
            <input type="checkbox" name="languages" value="jsp">JSP
            <input type="checkbox" name="languages" value="asp">ASP
            
            <br>
            <input type="hidden" name="FormName" value="SimpleForm">
            <input type="submit" name="enviar" value="Enviar">
            <input type="reset" name="Reset" value="Reset">
        </form>
        
        <%
            // $_POST["username"]
            String user = request.getParameter("username");
        %>
        
        <br>Method: <%= request.getMethod() %>
        <br>Query String: <%= request.getQueryString() %>
        <br>L'usuari introduït es: <%= user %>
        <br>Email Address: <%= request.getParameter("email") %>
        <br>Password: <%= request.getParameter("password") %>
        <br>Comments: <%= request.getParameter("comments") %>
        <br>Email Format: <%= request.getParameter("emailformat") %>
        <br>Occupation: <%= request.getParameter("occupation") %>
        <br>Other Languages:
        <%
            String[] languages = request.getParameterValues("languages");
            if(languages!=null) {
                for(String lang : languages) {
                    out.println(lang+" ");
                }
            }
        %>
        <br>FormName: <%= request.getParameter("FormName") %>
        <br>SessionID: <%= request.getRequestedSessionId() %>
    </body>
</html>
