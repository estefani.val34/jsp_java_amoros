<%-- 
    Document   : exercici03
    Created on : Oct 30, 2017, 10:58:39 AM
    Author     : alumne
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<jsp:include page="inici.jsp"></jsp:include>
        
        <form method="post">
            DNI: <input type="text" name="dni">
            <input type="submit" name="enviar" value="Comprovar">
        </form>
        
        <%
            String lletres = "TRWAGMYFPDXBNJZSQVHLCKE";
            
            if(request.getParameter("enviar")!=null) {
                String dni = request.getParameter("dni");
                if(dni.length()==9) {
                    String lletradni = dni.substring(8).toUpperCase();
                    String numerodni = dni.substring(0, 8);
                    int valor = Integer.parseInt(numerodni) % 23;
                    String lletracalculada = lletres.substring(valor, valor+1);

                    if(lletradni.equals(lletracalculada)) {
%>
                        <jsp:forward page="comprovar.jsp">
                            <jsp:param name="dni" value="ok" />
                        </jsp:forward>
<%
                        
                        
                    } else {
%>
                        <jsp:forward page="comprovar.jsp">
                            <jsp:param name="dni" value="ko" />
                        </jsp:forward>
<%

                    }
                }
            }
            
            
        %>
    </body>
</html>
