package def;
import java.util.HashMap;
import java.util.Map;

public class LlistaCursos {

	Map<String,String> llistaCursos;
	
	public LlistaCursos() {
		llistaCursos = new HashMap<String,String>();
		llistaCursos.put("c1","Metodologies fr�gils");
		llistaCursos.put("c2","Servidors de descontrol i perversions");
		llistaCursos.put("c3","Loser Experience");	
	}
	
	public String getCursById(String id ) {
		return llistaCursos.get(id);
	}
}
