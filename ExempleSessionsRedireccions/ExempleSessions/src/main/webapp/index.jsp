<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Adquireix els nostres cursos - Pas 1.</title>
</head>
<body>
	<h1>Adquireix els nostres cursos - Pas 1.</h1>
	<form action="" method="post">
		Introdueix...
		Nom: <input type="text" name="nom">
		<br/>
		Edat: <input type="number" name="edat" min="0" max="150">
		<br/>
		<input type="submit" name="btnContinuar" value="Continuar">
	</form>
        <% 
            if(request.getParameter("btnContinuar")!=null) {
                // Validar si estan plens o no.
                boolean validNom = false;
                boolean validEdat = false;
                String edat = request.getParameter("edat");
                int e = Integer.parseInt(edat);
                out.println("edat="+e);
                
                if(e<18) {
                   out.println("Has de ser major d'edat");
                } else {
                    // Guarda request
                    request.getRequestDispatcher("pas2.jsp").forward(request, response);
                    // Mal, no guarda request
                    // response.sendRedirect("pas2.jsp");
                }
            }
        %>
</body>
</html>