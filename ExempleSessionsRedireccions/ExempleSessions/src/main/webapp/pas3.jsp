<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="def.LlistaCursos" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Adquireix els nostres cursos - Pas 3.</title>
</head>
<body>
<% 
    String snom = (String) session.getAttribute("snom");
	LlistaCursos lc = new LlistaCursos();
	String select[] = request.getParameterValues("cursos"); 
	String selectedCourses = " ";
	if (select != null && select.length != 0) {
		for (int i = 0; i < select.length; i++) {
			selectedCourses = selectedCourses + lc.getCursById(select[i]) + ";"; 
		}
	}
	request.setAttribute("idCursos",selectedCourses); 
%>
	<h1>Adquireix els nostres cursos - Pas 3.</h1>
	<form action="welcome3.jsp" method="post">
		<p>Has triat els cursos: <%=selectedCourses %> </p> 
		<p>Est�s d'acord amb la teva decisi�, <%=snom %> ?? </p>
		<input type="submit" value="Acceptar">
	</form>
</body>
</html>